##### Prompt

Given a two-dimensional array that contains only three values, "H" for hallway, "_" for wall, and exactly one "G" for goal. This represents a maze. The entrance to the maze is always in the top left corner at index 0, 0. Return an array which is a list of (row, column) index pairs that represent a path from the entrance to the goal such that all of the values are "H". No ghosts walking through walls, please. Each move must be horizontal or vertical, no diagonals.

 

Here’s an example maze:

const exampleMaze = [
  ['H', 'H', 'H', '_', '_', 'H', 'H', 'H', 'G'],
  ['H', '_', 'H', '_', '_', 'H', '_', '_', '_'],
  ['H', '_', 'H', 'H', '_', 'H', '_', 'H', 'H'],
  ['_', '_', '_', 'H', '_', 'H', '_', 'H', '_'],
  ['H', 'H', 'H', 'H', '_', 'H', '_', 'H', '_'],
  ['H', '_', '_', 'H', '_', 'H', '_', 'H', 'H'],
  ['H', 'H', '_', '_', '_', 'H', 'H', 'H', '_'],
  ['H', '_', 'H', 'H', 'H', 'H', '_', '_', 'H'],
  ['H', 'H', 'H', '_', '_', 'H', 'H', 'H', 'H'],
];

function mazeSolver(maze) {
  /* your code here */
}