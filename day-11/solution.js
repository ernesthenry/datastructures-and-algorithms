function treeHeight(root) {
    // Base case
    if (root === null) {
      return 0;
    }
  
  
    // Try all possible branches (in a binary tree there are at most two)
    const leftHeight = treeHeight(root.left);
    const rightHeight = treeHeight(root.right);
  
  
    // Combine results from subproblems
    return 1 + Math.max(leftHeight, rightHeight);
  }