class LLNode {
    constructor(value, next = null, sublist = null) {
      this.value = value
      this.next = next
      this.sublist = sublist
    }
  }
  
  
  function flattenSublist(inputList) {
    // Define our output array
    const output = [];
  
  
    // Define a helper for traversing our list recursively
    function dfs(node) {
      if (!node) {
        return;
      }
  
  
      // Add present node value
      if (node.value) {
        output.push(node.value);
      }
  
  
      // Traverse sublist (after updating w/ present node value)
      dfs(node.sublist);
  
  
      // Continue on to the next node recursively
      dfs(node.next);
    }
  
  
    // Traverse from our input/root node
    dfs(inputList);
  
  
    return output;
  }
  