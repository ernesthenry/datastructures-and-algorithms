function daysToHigherTemp(dailyHighs) {
    const output = [];
  
  
    for (let day = 0; day < dailyHighs.length; day++) {
      const highTemp = dailyHighs[day];
      let count = 0;
  
  
      // Now loop forward from the day after and look for a higher
      // temperature.
      for (let i = day + 1; i < dailyHighs.length; i++) {
        if (dailyHighs[i] > highTemp) {
          // If we find one, record how many days forward we
          // had to look and then break out of this loop.
          count = i - day;
          break;
        }
      }
  
  
      // Add the count to the output.
      output.push(count);
    }
  
  
    return output;
  }

  